/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteanagrama;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author Augusto
 */
public class ClienteAnagrama {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, IOException {
        String statusJogador = "CLASSIFICADO";
        System.out.println("Digite o ip do servidor: ");
        Scanner teclado = new Scanner(System.in);
        String ipServidor = teclado.nextLine();
        Socket cliente = new Socket("127.0.0.1", 60000);
        System.out.println("Conectado ao servidor!");
        System.out.println("Aguarde até que os outros jogadores se conectem para que o jogo se inicie.");
        
        PrintStream saida = new PrintStream(cliente.getOutputStream());
        
        Scanner s = new Scanner(cliente.getInputStream());
        
        while("CLASSIFICADO".equals(statusJogador)){
            while (s.hasNextLine()) {
                System.out.println(s.nextLine());
                break;
            }
            saida.println(teclado.nextLine());
            while (s.hasNextLine()) {
                
                String respostaServidor = s.nextLine();
                System.out.println(respostaServidor);
                if(!"ERROU".equals(respostaServidor)){
                    statusJogador = respostaServidor;
                } else if("DESCLASSIFICADO".equals(respostaServidor)){
                    System.exit(0);
                } else{
                    System.out.println("Tente novamente: ");
                }
                break;
            }
        }
        

        saida.close();
        teclado.close();
        cliente.close();
    }
}
