package servidoranagrama;

import java.util.ArrayList;


/**
 *
 * @author Matheus
 */
public class GerenciadorClassificacao {
    private ArrayList<Thread> arrayJogadores;
    private ArrayList<Integer> arrayClassificados = new ArrayList<>();
    private GerenciadorPalavras gerenciadorPalavras;
    private String[] palavraAnagrama;
    int numTotalJogadores;
    int numJogadoresClassificados = 0;
    private String vencedor = null;

    public GerenciadorClassificacao(GerenciadorPalavras gerenciadorPalavras, int numTotalJogadores, ArrayList<Thread> arrayJogadores) {
        this.gerenciadorPalavras = gerenciadorPalavras;
        this.numTotalJogadores = numTotalJogadores;
        this.arrayJogadores = arrayJogadores;
        
        iniciarNovaRodada();
    }
    
    
    
    public String conferirPalavra(String respostaJogador, int numJogador){
        System.out.println(vencedor);
        System.out.println(numJogadoresClassificados);
        System.out.println(numTotalJogadores);
        if(numJogadoresClassificados == numTotalJogadores -1){
            numTotalJogadores --;
            if(numJogadoresClassificados == numTotalJogadores) {
                iniciarNovaRodada();
                for(Thread t: arrayJogadores){
                    try{
                        t.interrupt();
                    } catch(Exception e){}
                }
            }
            
            return "DESCLASSIFICADO";
            
        }else if(vencedor == null){
            if(this.palavraAnagrama[0].equals(respostaJogador) && numJogadoresClassificados < numTotalJogadores -1){
                this.numJogadoresClassificados ++;
                this.arrayClassificados.add(numJogador);
                
                if(numTotalJogadores == 2){
                    vencedor = "JOGADOR " + numJogador;
                    return "VENCEDOR";
                }

                return "CLASSIFICADO";

            } else if(!this.palavraAnagrama[0].equals(respostaJogador) && numTotalJogadores - numJogadoresClassificados >= 2){
                return "ERROU";
            } else{
                return "DESCLASSIFICADO";
            }
        } else{
            return "Tarde demais. " + vencedor + " venceu. FIM DE JOGO.";
        }
    }
    
    public void iniciarNovaRodada(){
        this.palavraAnagrama = this.gerenciadorPalavras.getPalavraAnagrama();
        if(this.numJogadoresClassificados > 0){
            this.numTotalJogadores = this.numJogadoresClassificados;
            this.numJogadoresClassificados = 0;
        }
    }
    
    public String getAnagrama(){
        return "Resolva o seguinte anagrama: " + this.palavraAnagrama[1];
    }
    
}
