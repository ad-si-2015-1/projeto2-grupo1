package servidoranagrama;

import java.util.*;

public class GerenciadorPalavras {

    private List<String> palavras = getPalavras();

    public String obtenhaPalavraAleatoriamente() {
        Integer casaAleatoria = new Random().nextInt(((this.palavras.size() - 1) - 0) + 1) + 0;
        //System.out.println(casaAleatoria);
        return palavras.get(casaAleatoria);
    }

    public String obtenhaAnagrama(String palavra) {
        List<String> listaPalavra = Arrays.asList(palavra.split(""));
        Collections.shuffle(listaPalavra);
        String anagrama = "";
        for (String letra : listaPalavra) {
            anagrama += letra;
        }
        return anagrama;
    }

    public String[] getPalavraAnagrama() {
        String palavra = obtenhaPalavraAleatoriamente();
        String anagrama = obtenhaAnagrama(palavra);
        String[] palavraAnagrama = {palavra, anagrama};
        return palavraAnagrama;
    }

    private List<String> getPalavras() {
        List<String> palavras = new ArrayList<String>() {
            {
                add("anagrama");
                add("gerenciador");
                add("servidor");
                add("palavra");
                add("universidade");
                add("batuta");
                add("europa");
            }
        };
        return palavras;
    }


}