package servidoranagrama;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Augusto
 */
public class ServidorAnagrama {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final int PORTA_PADRAO = 60000;
        int numJogadores;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("ANAGRAMA");
        System.out.println("Iniciando...");
        System.out.println("\n\nPor favor indique o número de jogadores: ");
        
        numJogadores = sc.nextInt();
        
        while(numJogadores <= 1){
            System.out.println("É preciso pelo menos 2 jogadores. Digite outro valor: ");
            numJogadores = sc.nextInt();
        }

        System.out.println("O jogo se iniciará quando os "+numJogadores+" se conectarem.");
        GerenciadorPalavras gerPalavras = new GerenciadorPalavras();
        ArrayList<Thread> arrayJogadores = new ArrayList<>();
        GerenciadorClassificacao gerClassificacao = new GerenciadorClassificacao(gerPalavras, numJogadores, arrayJogadores);
        GerenciadorConexoes gerConexoes = new GerenciadorConexoes(PORTA_PADRAO, numJogadores, gerClassificacao, arrayJogadores);
    }
}
