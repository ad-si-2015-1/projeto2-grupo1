package servidoranagrama;

import java.util.ArrayList;


/**
 *
 * @author Matheus
 */
public class GerenciadorClassificacao {
    private ArrayList<Thread> arrayJogadores;
    private GerenciadorPalavras gerenciadorPalavras;
    private String[] palavraAnagrama;
    int numTotalJogadores;
    int numJogadoresClassificados = 0;
    private String vencedor = null;

    public GerenciadorClassificacao(GerenciadorPalavras gerenciadorPalavras, int numTotalJogadores, ArrayList<Thread> arrayJogadores) {
        this.gerenciadorPalavras = gerenciadorPalavras;
        this.numTotalJogadores = numTotalJogadores;
        this.arrayJogadores = arrayJogadores;
        
        iniciarNovaRodada();
    }
    
    
    
    public String conferirPalavra(String respostaJogador, int numJogador){
        if(vencedor == null){
            if(this.palavraAnagrama[0].equals(respostaJogador) && numJogadoresClassificados < numTotalJogadores){
                this.numJogadoresClassificados ++;

                if(numTotalJogadores == 2){
                    vencedor = "JOGADOR " + numJogador;
                    return "VENCEDOR";
                } else if(this.numJogadoresClassificados == numTotalJogadores - 1){
                    iniciarNovaRodada();
                    for(Thread t: arrayJogadores){
                        try{
                            t.interrupt();
                        } catch(Exception e){}
                    }
                }

                return "CLASSIFICADO";

            } else if(!this.palavraAnagrama[0].equals(respostaJogador) && numTotalJogadores - numJogadoresClassificados >= 2){
                return "ERROU";
            } else{
                return "DESCLASSIFICADO";
            }
        } else{
            return "Tarde demais. " + vencedor + " venceu. FIM DE JOGO.";
        }
    }
    
    public void iniciarNovaRodada(){
        this.palavraAnagrama = this.gerenciadorPalavras.getPalavraAnagrama();
        if(this.numJogadoresClassificados > 0){
            this.numTotalJogadores = this.numJogadoresClassificados;
            this.numJogadoresClassificados = 0;
        }
    }
    
    public String getAnagrama(){
        return "Resolva o seguinte anagrama: " + this.palavraAnagrama[1];
    }
    
}
