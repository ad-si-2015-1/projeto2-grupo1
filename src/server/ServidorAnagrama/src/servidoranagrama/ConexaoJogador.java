package servidoranagrama;


import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Matheus
 */
public class ConexaoJogador implements Runnable {

    private int numJogador;
    private GerenciadorClassificacao gerClassificacao;
    private GerenciadorConexoes gerConexoes;
    Socket conexao;
    String status = "INICIANDO";
    Thread thread;
    
    public void setThread(Thread t){
        this.thread = t;
    }
    
    public ConexaoJogador(GerenciadorClassificacao gerClassificacao, GerenciadorConexoes gerConexoes, int numeroJogador, Socket conexao) {
        this.gerClassificacao = gerClassificacao;
        this.gerConexoes = gerConexoes;
        this.numJogador = numeroJogador;
        this.conexao = conexao;
    }

    @Override
    public void run() {
        try {
            System.out.println("Jogador "+this.numJogador+" conectado!");
            System.out.println("Ip do jogador "+this.numJogador+": " + conexao.getInetAddress().getHostAddress());
            
            try{
                Scanner s = null;
                String anagramaVelho = "";
                while("INICIANDO".equals(this.status) || "CLASSIFICADO".equals(this.status) || "ERROU".equals(this.status)){
                    PrintStream saida = new PrintStream(conexao.getOutputStream());
                    saida.println( gerClassificacao.getAnagrama() );

                    s = new Scanner(conexao.getInputStream());
                    String respostaJogador = null;
                    while(s.hasNextLine()){
                        respostaJogador = s.nextLine();
                        break;
                    }
                    if(respostaJogador != null){
                        System.out.println("Resposta recebida do jogador "+this.numJogador+ ": "+respostaJogador);

                        String resultado = gerClassificacao.conferirPalavra(respostaJogador, this.numJogador);
                        saida.println( resultado );

                        this.status = resultado;
                        try{
                            if(resultado.equals("CLASSIFICADO")) Thread.sleep(999999999);
                        }catch(InterruptedException ie){}

                    }
                }

                if("DESCLASSIFICADO".equals(this.status)){
                    //AVISA QUE FOI DESCLASSIFICADO
                    System.out.println("Jogador "+numJogador+" foi desclassificado.");
                }

                if("VENCEDOR".equals(this.status)){
                    //AVISA QUE VENCEU
                    System.out.println("Jogador "+numJogador+" venceu o jogo!");
                }

                //ENCERRA A CONEXAO
                if(s != null) s.close();
                conexao.close();

            }catch(Exception e){
                Logger.getLogger(ConexaoJogador.class.getName()).log(Level.SEVERE, null, e);
            }
            
        } catch (Exception ex) {
            Logger.getLogger(ConexaoJogador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void iniciarJogo(){
        
    }
    
    public int getNumJogador(){
        return this.numJogador;
    }

}
