package servidoranagrama;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Augusto
 */
public class GerenciadorConexoes {
    private ArrayList<Thread> arrayJogadores;
    private int portaPadrao;
    private GerenciadorClassificacao gerClassificacao;
    private int numMaxJogadores;
    private int numJogadoresConectados;
    ServerSocket servidor;

    public GerenciadorConexoes(int portaPadrao, int numMaxJogadores, GerenciadorClassificacao gerClassificacao, ArrayList<Thread> arrayJogadores) {
        try {
            this.portaPadrao = portaPadrao;
            this.numMaxJogadores = numMaxJogadores;
            this.gerClassificacao = gerClassificacao;
            servidor = new ServerSocket(this.portaPadrao);
            this.arrayJogadores = arrayJogadores;
            
            novaConexao(1);
            
        } catch (IOException ex) {
            Logger.getLogger(GerenciadorConexoes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void novaConexao(int numJogador){
        try {
            ConexaoJogador cJogador = new ConexaoJogador(gerClassificacao, this, numJogador, servidor.accept());
            Thread threadJogador = new Thread(cJogador);
            cJogador.setThread(threadJogador);
            
            this.numJogadoresConectados++ ;
            this.arrayJogadores.add(threadJogador);
            if(numJogadoresConectados == numMaxJogadores){
                for(Thread thrJogador: this.arrayJogadores){
                    thrJogador.start();
                }
            } else{
                novaConexao(numJogador + 1);
            }
        } catch (IOException ex) {
            Logger.getLogger(GerenciadorConexoes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
